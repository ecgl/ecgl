# Ellis Coulson - Personal README.md

I'm Ellis, here are some things you should know about me.

## My Working Hours

I have a tendency to work non-linear days, but will usually be available between `10:00 - 19:00 GMT/BST` (UK time), unless I am designated the SEOC when I will be available between `07:00 - 15:00 UTC`. I am not typically a morning person, I need a little time (and quite a bit of coffee) to really get going in the morning. I'd prefer if meetings were after 10:00 (AM) UK time. That said, I'm still very happy to meet & communicate in the mornings especially if we have no viable alternatives, timezones being as they are.

## Communicating With Me

I generally prefer to communicate asynchronously, unless it would be impractical to do so.

### Communication Methods

#### **Slack**: 

Feel free to DM me on Slack if you need anything - Slack is usually the first thing I'll check, so if you need to get ahold of me quickly it is likely your best bet. 

I'd prefer Slack to be used for more ad-hoc / quick communication, if we have a big problem to solve together, let's create a GitLab issue or merge request instead!

#### **Email**: 

I generally dislike email, and don't check it as frequently as Slack or my GitLab to-do's.

#### **GitLab**: 

Where longer-form communication is needed I'd prefer to use GitLab.com. I also tend to use GitLab to-do's a lot to plan my work, so either mention me (`@ecgl`) in an issue, or a merge request.

#### Video Calls & Meetings

As much as I prefer to communicate asynchronously, it is sometimes just impractical to do so. If you feel that is the case please feel free to schedule a meeting with me.

It's also nice to connect from time-to-time over a video call, so please feel free to schedule coffee chats with me if you'd just like to have a chat!

In order of preference, I like to collaborate with:

1. GitLab To-Dos
2. Instant messaging / Slack
3. Phone/video calls
4. Email

### Communication Style

I'm quite open about having ASD (Autistic Spectrum Disorder); the reason I mention this is that 'reading between the lines' isn't always intuitive for me. I think the [GitLab handbook actually puts it perfectly](https://handbook.gitlab.com/handbook/values/#embracing-neurodiversity) - `provide very explicit and precise instructions and due-dates when giving tasks`. The more direct & precise, the better.

Regardless of how we choose to collaborate, I'm most definitely a context enjoyer; if you're asking me for something, I may ask you for more context, and really appreciate when you provide it. On the flip side, if I'm working with you on something, I may be providing too much context to a point where I'm just needlessly info-dumping... if that is the case please tell me so! I don't do it on purpose!

## Hobbies & Interests

Outside of work I enjoy coding, playing the guitar & drums, and playing video games.